var selectedId = null;
// selectedId ~ id của product được chọn để edit

// tạo function renderProduclist(productArr): nhận vào 1 array và render array lên layout
function renderProductList(productArr) {
  var contentHTML = "";
  for (var i = 0; i < productArr.length; i++) {
    var product = productArr[i];
    var trString = `<tr>
                        <td>${product.id}</td>
                        <td>${product.name}</td>
                        <td>${product.price}</td>
                        <td>${product.image}</td>
                        <td>${product.desc}</td>
                        <td>
                            <button class='btn btn-warning'
                            onclick=editProduct(${product.id})
                            >Edit
                            </button>
                            <button onclick=deleteProduct(${product.id}) class='btn btn-danger'>Delete</button>
                        </td>
                     </tr>`;
    contentHTML += trString;
  }
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}
function fetchFoodList() {
  turnOnLoading();
  // gọi api lấy danh sách sản phẩm đang có từ server
  axios({
    url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      // api trả data về thành công
      renderProductList(res.data.reverse());
    })
    .catch(function (err) {
      turnOffLoading();

      console.log("😀 - err", err);
    });
}
fetchFoodList();
// xoá 1 món ăn
function deleteProduct(id) {
  turnOnLoading();
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log("😀 - deleteProduct - xoá thành công", res);
      fetchFoodList(); // gọi lại api lấy danh sách mới nhất từ server SAU khi xoá thành công
    })
    .catch(function (res) {
      turnOffLoading();
      console.log("xoá thất bại");
    });
}
// thêm product: gửi data lên server
function addProduct() {
  // lấy thông tin từ form
  var newProduct = getDataForm();
  // gọi api và đưa newProduct lên server
  axios({
    url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
    method: "POST",
    data: newProduct,
  })
    .then(function (res) {
      fetchFoodList();
    })
    .catch(function (err) {
      console.log("😀 - err", err);
    });
}

function editProduct(id) {
  selectedId = id;
  //lấy thông tin chi tiết 1 product từ server dựa trên id
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      $("#myModal").modal("show");
      // đưa data từ server về lên form
      showDataForm(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}
function updateProduct() {
  /**
   *
   * 1.lấy data vừa được update bởi user ( form )
   * 2. gọi axios, đưa data lên server
   * 3. render lại layout vs data mới nhất
   */
  var product = getDataForm();
  turnOnLoading();
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${selectedId}`,
    method: "PUT",
    data: product,
  })
    .then(function (res) {
      $("#myModal").modal("hide");
      fetchFoodList();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}

// pendding, success, fail

// bật loading 1 lần

// tắt loading 2 lần
