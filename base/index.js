// đồng bộ ~ bất đồng bộ
console.log(2);
console.log(1);

setTimeout(function () {
  console.log("bật quảng cáo");
}, 5000);

axios({
  url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
  method: "GET",
})
  .then(function (res) {
    // response ~ phản hồi từ BE
    console.log("😀 - res", res);
  })
  .catch(function (err) {
    console.log("😀 - err", err);
    // error ~ lỗi trả về từ BE nếu có
  });

// 3 trạng thái: pendding, resolve (success), reject ( fail)

/**
 * 
GET ~ lấy tất cả || chi tiết 
POST ~ tạo mới
PUT ~ update 
DELETE ~ xoá
*/
